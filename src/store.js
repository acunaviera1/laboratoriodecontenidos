import Vue from 'vue'
import Vuex from 'vuex'
// import { isContext } from 'vm';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    modal: false
  },
  mutations: {
    SET_MODAL: (state, modal) => {
      state.modal = modal
    },
    TOGGLE_MODAL: (state) => {
      state.modal != state.modal
    }
  },
  actions: {
    setModal: (context, modal) => {
      context.commit("SET_MODAL", modal)
    },
    toggleModal: (context) => {
      context.commit("TOGGLE_MODAL")
    }
  },
  getters: {
    getModalStatus: (state) => {
      return state.modal;
    }
  }
})
